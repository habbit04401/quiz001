package edu.cyut.javaclass;


import java.util.Scanner;

/**
 * 請撰寫一個Java程式，可以算出二個不超過100的正整數，它們的最大公因數及最小公倍數（請勿使用）。程式需求如下：
 * 當輸入數值不合規定時，會提示錯誤訊息，範例如下。
 * 第1個數值（xxx）不是合法數值。
 * 第2個數值（xxx）不是合法數值。
 * 二個數值都不是正整數。
 * 輸出字串範例如下：
 * xxx與xxx的最大公因數是xxx，最小公倍數是xxx。
 */
public class Main {

    public static void main(String[] args) {
	// write your code here

        Scanner scanner = new Scanner(System.in);
        int b= 0 ;
        int sum1 =scanner.nextInt();
        int sum2 =scanner.nextInt();
        if (sum1 >= 100 ){
            System.out.println("第1個數值"+sum1+"不是合法數值");}
            if (sum2 >=100){
                System.out.println("第2個數值"+sum2+"不是合法數值");
            }
            if (sum2>0 && sum1<=100 && sum2>0 && sum2<=100 )
            System.out.print(sum1+"與"+sum2+"的最大公因數是");
            for(int a=2; a<=sum1;a++){
                if(sum1 %a ==0 && sum2 %a ==0)
                    b=a;
            }
            //最小公倍數
            int tmp = 0, in1 = sum1, in2 = sum2,x=1;
            while(sum1 % sum2 != 0){
            tmp = sum2;
            sum2 = sum1 % sum2;
            sum1 = tmp;
            }
            x=in1 * in2 / sum2;
            System.out.println(b+"，最小公倍數是"+x);

    }
}
